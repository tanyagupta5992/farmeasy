const http = require('http');
const express = require('express');
const cors = require('cors');
require('./dbConfig');

const app = express();
const port = process.env.PORT || 8080;
app.set('port',port);
app.use(express.json());
app.use(cors());

/* setting up the server */

const server = http.createServer(app);
server.on('listening', function() {
    console.log('Listening on port '+port);
});
server.on('error', function(error) {
    if(error.code == 'EACCES') {
        console.log('requires higher privileges');
    }
    if(error.code == 'EADDRINUSE') {
        console.log('port is already in use');
    }
});
server.listen(port);

/* api routing */
const usersRouter = require('./controllers/usersController');
app.use('/user',usersRouter);

module.exports = app;