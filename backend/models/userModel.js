const mongoose = require('mongoose');

const usr=new mongoose.Schema({
    _id: {type: String, required: false},
    usr_id: {type: String, required: false},
    first_name: {type: String, required: true},
    middle_name: {type: String, required: false},
    last_name: {type: String, required: true},
    email: {type: String, required: true},
    account_active: {type: Boolean, required: false},
    password: {type: String, required: false},
    termsAndConditions : {type: Boolean, required:false},
    create_ts: {type: String, required: false},
    created_by: {type: String, required: false},
    update_ts: {type: String, required: false},
    modified_by: {type: String, required: false}    
},{collection : 'usr'});

module.exports = mongoose.model('usr',usr);  
