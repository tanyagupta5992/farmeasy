const express = require('express');
const router = express.Router();
const Usr = require('../models/userModel');

router.get('/exists/:email', async(req,res) => {
    try {
       const result = await Usr.exists({email: req.params.email})
       res.statusCode = 200;
       res.json(result);
    } catch(err) {
       res.statusCode = 500;
       res.send('Error: '+err);
    }
})

router.post('/login', async(req,res) => {
    try {
       console.log(req.body);
       const userResult = await Usr.findOne({email: req.body.email, password: req.body.password})
       if(userResult != null) {
         res.json('Login Successful');
       }
       else {
         res.json('Invalid username or password');
       }
    } catch(err) {
       res.send('Error: '+err);
    }
})

router.post('/save', async(req,res) => {
    const userWithMaxId = await Usr.find().sort({_id: -1}).limit(1)
    var maxId = parseInt(userWithMaxId[0]._id)+1;
    const usr = new Usr({
    _id: maxId,
    usr_id: maxId,
    first_name: req.body.first_name,
    middle_name: req.body.middle_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
    termsAndConditions: req.body.termsAndConditions,
    account_active: true,
    create_ts: Date.now(),
    created_by: 'system',
    update_ts: Date.now(),
    modified_by: 'system'
    })
    try {
       const result = await usr.save()
       res.statusCode = 200;
       res.json(result);
    } catch(err) {
       res.statusCode = 500;
       res.send('Error: '+err);
    }
})

module.exports = router;