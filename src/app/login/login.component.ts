import { Component, OnInit } from '@angular/core';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';
import { LoginStatusService } from '../services/status.service';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private socialAuthService: SocialAuthService, private http:HttpClient,private router:Router, private loginStatusService: LoginStatusService) { }

  newUser: boolean = false;
  user = {
    email: '',
    password: ''
  } 
  signupData = {
    first_name: '',
    middle_name: '',
    last_name: '', 
    email: '',
    password: '',
    confirmPassword: '',
    termsAndConditions: false
  } 
  socialUser: SocialUser = new SocialUser;
  isLoggedin: boolean = false;
  errorMsg = '';
  successMsg = '';
  emailExists: any;
  url = 'http://localhost:8080/';

  ngOnInit(): void {    
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
    });
  }

  validateSignUp(signupForm: NgForm) {
    if(signupForm.valid) {
      this.errorMsg='';
      if(this.signupData.password != this.signupData.confirmPassword) {
        this.errorMsg = 'Password does not match. Please try again'
      }
      else {
          this.checkEmailExists(signupForm,this.signupData.email);
      }
    }
  }

  checkEmailExists(signupForm : NgForm,email:String) {
    this.http.get(this.url+'user/exists/'+email)
    .subscribe(res => {
      this.emailExists = res;
      if(this.emailExists == true) {
        this.errorMsg = 'This email address already exists in our system';
      }
      else {
        this.signup(signupForm);
      }
    })
  }

  checkGoogleAccountExists(email: String) {
    this.http.get(this.url+'user/exists/'+email)
    .subscribe(res => {
      this.emailExists = res;
      if(this.emailExists == true) {
        this.loginStatusService.setLoginStatus(true);
        this.router.navigate(['home']);
      }
      else {
        this.signupData = {
          first_name: this.socialUser.firstName,
          middle_name: '',
          last_name: this.socialUser.lastName, 
          email: this.socialUser.email,
          password: '',
          confirmPassword: '',
          termsAndConditions: true
        }
        this.addGoogleAccount(this.signupData);
      }
    })  
  }

  addGoogleAccount(signupData: any) {
    this.http.post(this.url+'user/save',this.signupData)
    .subscribe(res => {
      this.successMsg = 'User has been registered successfully!';
      this.user = {
        email: this.signupData.email,
        password: this.signupData.password
      }
      this.loginStatusService.setLoginStatus(true);
      this.router.navigate(['home']);
    })
  }

  signup(signupForm : NgForm) {
    this.http.post(this.url+'user/save',this.signupData)
    .subscribe(res => {
      this.successMsg = 'User has been registered successfully!';
      this.user = {
        email: this.signupData.email,
        password: this.signupData.password
      }
      this.login(signupForm);
    })
  }

  login(loginForm: NgForm) {
    if(loginForm.valid) {
      this.http.post(this.url+'user/login',this.user)
      .subscribe(res => {
        if(res == 'Login Successful') {
          this.loginStatusService.setLoginStatus(true);
          this.router.navigate(['home']);
        }
        else {
          this.errorMsg = 'Invalid username or credentials';
        }
      })
    }
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
    .then(res => {
        this.socialUser = res;
        this.checkGoogleAccountExists(this.socialUser.email);
    });
  }    
}
