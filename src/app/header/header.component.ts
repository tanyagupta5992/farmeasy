import { Component, OnInit } from '@angular/core';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';
import { LoginStatusService } from '../services/status.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private socialAuthService: SocialAuthService, private router:Router, private loginStatusService: LoginStatusService) { }

  profileOptions: boolean=false;
  loginStatus: boolean=false;

  ngOnInit(): void {
    this.loginStatusService.loginStatus$.subscribe(loginStatus =>
      {
          this.loginStatus=loginStatus;
      });
  }

  logout(): void {
    this.socialAuthService.signOut()
    .then(res => {
      this.loginStatus=false;
      this.router.navigate(['']);
    })
  }

}
