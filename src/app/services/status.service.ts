import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class LoginStatusService {
  
  constructor() { }

  private loginStatus: boolean = false;
  loginStatus$ = new BehaviorSubject(this.loginStatus);

  setLoginStatus(status: boolean) {
    this.loginStatus$.next(status);
  }

}